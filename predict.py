import sys

import torch
import torch.optim as optim
import numpy as np
from allennlp.data import Instance
from allennlp.data.fields import TextField, SequenceLabelField
from allennlp.common.file_utils import cached_path
from allennlp.data.tokenizers import Token
from allennlp.data.vocabulary import Vocabulary
from allennlp.models import Model
from allennlp.modules.text_field_embedders import BasicTextFieldEmbedder
from allennlp.modules.token_embedders import Embedding
from allennlp.modules.seq2seq_encoders import Seq2SeqEncoder, PytorchSeq2SeqWrapper
from allennlp.nn.util import get_text_field_mask, sequence_cross_entropy_with_logits
from allennlp.training.metrics import CategoricalAccuracy
from allennlp.data.iterators import BucketIterator
from allennlp.training.trainer import Trainer

# Imports in addition to the tutorial
from allennlp.models.encoder_decoders.copynet_seq2seq import CopyNetSeq2Seq
from allennlp.data.dataset_readers.copynet_seq2seq import CopyNetDatasetReader
from allennlp.predictors import Seq2SeqPredictor, Predictor
from allennlp.modules.attention import DotProductAttention
from allennlp.training.metrics.bleu import BLEU
import pickle
from collections import defaultdict
from nltk.translate.bleu_score import sentence_bleu, corpus_bleu
import apex

if __name__== "__main__":
    args = sys.argv[1:]
    model_name = args[0]
    path = args[1]
    
    # Get and format dataset (the 30k file) - into train validation
    reader = CopyNetDatasetReader(target_namespace = 'target_tokens')
    
    # Load the model
    vocab = Vocabulary.from_files("./vocabulary")
    with open(model_name, 'rb') as input:
        model = pickle.load(input)
    
    print('Model:',model_name, model)
    predictor = Seq2SeqPredictor(model, dataset_reader=reader)        

    ref_dict = defaultdict(list)
    can_dict = defaultdict(list)
    formulas = []
    # Get all the references for each formula
    with open(path, 'r') as file:
        for line in file.readlines():
            formula = line.split('\t')[0].strip('\n')
            if formula not in formulas:
                formulas.append(formula)
            
    with open('predictions.txt', 'w') as file:
        for formula in formulas:
            prediction = predictor.predict(formula)                
            best_index = np.argmax(prediction['predicted_log_probs'])
            perspective = ' '.join(prediction['predicted_tokens'][best_index])
            file.write(formula+'\t'+perspective+'\n')
        