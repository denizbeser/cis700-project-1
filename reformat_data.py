import csv

files = ['generation_test.tsv','generation_train.tsv']

for file in files:
    with open('data/'+file, 'r') as inf:
        lines = inf.readlines()

    with open('data/reformatted_'+file, 'w') as outf:
        for i in range(1,len(lines)):
            tokens = lines[i].strip('\n').split('\t')
            print(tokens[-2]+'\t'+tokens[-1],file=outf)