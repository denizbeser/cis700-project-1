import sys

import torch
import torch.optim as optim
import numpy as np
from allennlp.data import Instance
from allennlp.data.fields import TextField, SequenceLabelField
from allennlp.common.file_utils import cached_path
from allennlp.data.tokenizers import Token
from allennlp.data.vocabulary import Vocabulary
from allennlp.models import Model
from allennlp.modules.text_field_embedders import BasicTextFieldEmbedder
from allennlp.modules.token_embedders import Embedding
from allennlp.modules.seq2seq_encoders import Seq2SeqEncoder, PytorchSeq2SeqWrapper
from allennlp.nn.util import get_text_field_mask, sequence_cross_entropy_with_logits
from allennlp.training.metrics import CategoricalAccuracy
from allennlp.data.iterators import BucketIterator
from allennlp.training.trainer import Trainer

# Imports in addition to the tutorial
from allennlp.models.encoder_decoders.copynet_seq2seq import CopyNetSeq2Seq
from allennlp.data.dataset_readers.copynet_seq2seq import CopyNetDatasetReader
from allennlp.predictors import Seq2SeqPredictor, Predictor
from allennlp.modules.attention import DotProductAttention
from allennlp.training.metrics.bleu import BLEU
import pickle
from collections import defaultdict
from nltk.translate.bleu_score import sentence_bleu, corpus_bleu
import apex
from sklearn.utils import resample

if __name__== "__main__":
    args = sys.argv[1:]
    model_name = args[0]    

    test_path = 'data/reformatted_generation_test.tsv'

    # Get and format dataset (the 30k file) - into train validation
    reader = CopyNetDatasetReader(target_namespace = 'target_tokens')
    
    # Load the model
    vocab = Vocabulary.from_files("./vocabulary")
    with open(model_name, 'rb') as input:
        model = pickle.load(input)
    
    print('Model:',model_name, model)
    predictor = Seq2SeqPredictor(model, dataset_reader=reader)        

    # BLEU Score for test and training data
    # for path in [test_path, train_path]:        
    path = test_path

    print('Reading test files...')

    ref_dict = defaultdict(list)
    can_dict = defaultdict(list)
    # Get all the references for each formula
    with open(path, 'r') as file:
        for line in file.readlines():
            formula, gold = line.split('\t')
            ref_dict[formula].append(gold.strip('\n').split())

    for formula in ref_dict.keys():
        prediction = predictor.predict(formula)                
        best_index = np.argmax(prediction['predicted_log_probs'])
        can_dict[formula] = prediction['predicted_tokens'][best_index]
    

    formulas = ref_dict.keys()
    references = [ref_dict[f] for f in formulas]
    candidates = [can_dict[f] for f in formulas]
    
    ##################################################
    # Bootstrap algorithm begins here
    ##################################################
    
    print('\nCalculating Bootstrap p-value for', path)
    
    m_alg_x = corpus_bleu(references, candidates)
    # m_alg_x = 0.6505
    baseline = 0.5732
    dx = m_alg_x - baseline
    data = list(zip(references, candidates))
    n = len(data) # 68

    s = 0
    b = 10**3 # 1000 gave p=0.001
    for i in range(b):
        # get n random indices, use those indices as test points
        xi = resample(data, replace=True, n_samples=n)
        xi_references, xi_candidates = tuple(zip(*xi))

        # perform evaluation on xi
        m_alg_xi = corpus_bleu(xi_references, xi_candidates)
        dxi = m_alg_xi - baseline        
        
        # increment s if difference is large enough
        if dxi > 2*dx:
            s+=1
        # print(i,s,dxi)

    print('p =',s/b)