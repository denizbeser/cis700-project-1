import sys

import torch
import torch.optim as optim
import numpy as np
from allennlp.data import Instance
from allennlp.data.fields import TextField, SequenceLabelField
from allennlp.common.file_utils import cached_path
from allennlp.data.tokenizers import Token
from allennlp.data.vocabulary import Vocabulary
from allennlp.models import Model
from allennlp.modules.text_field_embedders import BasicTextFieldEmbedder
from allennlp.modules.token_embedders import Embedding
from allennlp.modules.seq2seq_encoders import Seq2SeqEncoder, PytorchSeq2SeqWrapper
from allennlp.nn.util import get_text_field_mask, sequence_cross_entropy_with_logits
from allennlp.training.metrics import CategoricalAccuracy
from allennlp.data.iterators import BucketIterator
from allennlp.training.trainer import Trainer

# Imports in addition to the tutorial
from allennlp.models.encoder_decoders.copynet_seq2seq import CopyNetSeq2Seq
from allennlp.data.dataset_readers.copynet_seq2seq import CopyNetDatasetReader
from allennlp.predictors import Seq2SeqPredictor, Predictor
from allennlp.modules.attention import DotProductAttention
from allennlp.training.metrics.bleu import BLEU
import pickle
from collections import defaultdict
from nltk.translate.bleu_score import sentence_bleu, corpus_bleu
import apex

if __name__== "__main__":
    train_path = 'data/reformatted_generation_train.tsv'
    test_path = 'data/reformatted_generation_test.tsv'

    # Get and format dataset (the 30k file) - into train validation
    reader = CopyNetDatasetReader(target_namespace = 'target_tokens')
    
    # Parameters (from run_generation.sh and nn/src/py/main.py )
    IN_EMBEDDING_DIM = 50
    OUT_EMBEDDING_DIM = 50
    HIDDEN_DIM = 100
    EPOCHS = 20
    max_decoding_steps = 30

    # Form train and val datasets
    train_dataset = reader.read(train_path)
    validation_dataset = train_dataset[:len(train_dataset)//5]
    train_dataset = train_dataset[len(train_dataset)//5:]
    print('Train size:',len(train_dataset),'Val size:',len(validation_dataset))

    # Extract Vocab
    vocab = Vocabulary.from_instances(train_dataset + validation_dataset)

    i = 0        
    torch.manual_seed(i)

    # Make token embeddings
    token_embedding = Embedding(num_embeddings=vocab.get_vocab_size('tokens'),
                                embedding_dim=IN_EMBEDDING_DIM)
    word_embeddings = BasicTextFieldEmbedder({"tokens": token_embedding})

    # Setup Seq2Seq RNN (BiDirectional from the paper)
    encoder_bilstm = PytorchSeq2SeqWrapper(torch.nn.LSTM(IN_EMBEDDING_DIM, HIDDEN_DIM, batch_first=True, bidirectional = True))

    # Initialize CopyNet model built in the paper with Attention
    attention = DotProductAttention()
    model = CopyNetSeq2Seq(vocab, word_embeddings, encoder_bilstm, attention, 
        beam_size=8, max_decoding_steps=max_decoding_steps, 
        target_embedding_dim = OUT_EMBEDDING_DIM) 

    # Initiliaze optimizer, iterator, trainer
    optimizer = optim.SGD(model.parameters(), lr=0.1)
    iterator = BucketIterator(batch_size=32, sorting_keys=[("source_tokens", "num_tokens")])
    iterator.index_with(vocab)
    trainer = Trainer(model=model,
                      optimizer=optimizer,
                      iterator=iterator,
                      train_dataset=train_dataset,
                      validation_dataset=validation_dataset,
                      patience=10,
                      num_epochs=EPOCHS)
    # Train
    trainer.train()
    # Save the model
    vocab.save_to_files("./vocabulary")
    with open('models/model_'+str(i)+'.pkl', 'wb') as output:            
        pickle.dump(model, output, pickle.HIGHEST_PROTOCOL)